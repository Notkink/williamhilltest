package com.williamhill.testcases;

import com.williamhill.pages.HomePage;
import com.williamhill.pages.VerifyFoundedGame;
import com.williamhill.pages.VerifyPlayGame;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.Map;

public class VerifyWilliamHillTestCases {

    private String testURL = "https://vegas.williamhill.com/en-gb/";
    private WebDriver driver;


    @BeforeSuite
    @Parameters({"device"})
    public void setupSuite(@Optional("Chrome") String device) {

        switch (device) {
            case "iPhone": {

                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\resources\\webdrivers\\chromedriver.exe");
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "iPhone 5");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                driver = new ChromeDriver(chromeOptions);
                driver.navigate().to(testURL);

                break;
            }
            case "Android": {

                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\resources\\webdrivers\\chromedriver.exe");
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 5");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                driver = new ChromeDriver(chromeOptions);
                driver.navigate().to(testURL);

                break;
            }
            case "Firefox":

                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\resources\\webdrivers\\geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.navigate().to(testURL);

                break;
            case "Chrome":

                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\resources\\webdrivers\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.navigate().to(testURL);

                break;
             default : throw new SkipException("device " + device + " does not exist" );

        }

        WebDriverWait webDriverWait = new WebDriverWait(driver, 5);
        try {
            webDriverWait.until(w -> w.findElement(By.xpath("//button[contains(., 'Accept & Close')]"))).click();
        } catch (WebDriverException e) {
            System.err.println(e.getMessage());
        }

    }

    @Test(priority = 1)
    @Parameters({"gameName"})
    public void verifyWilliamHillHomePage(@Optional("Mayfair Roulette") String gameName) {


        HomePage homePage = new HomePage(driver);
        homePage.clickonMagniferButton();
        homePage.typeGameName(gameName);

    }

    @Test(priority = 2, dependsOnMethods = "verifyWilliamHillHomePage")
    @Parameters({"gameName"})
    public void verifyWilliamHillFoundedGame(@Optional("Mayfair Roulette") String gameName) throws InterruptedException {

        VerifyFoundedGame verifyFoundedGame = new VerifyFoundedGame(driver);
        verifyFoundedGame.hoverOverElement(gameName);
    }


    @Test(priority = 3, dependsOnMethods = "verifyWilliamHillFoundedGame")
    public void verifyPlayButton() {

        VerifyPlayGame verifyPlayGame = new VerifyPlayGame(driver);
        verifyPlayGame.clickPlay();
    }


    @AfterSuite
    public void teardownSuite() {
        driver.quit();
    }

}



