package com.williamhill.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

public class VerifyFoundedGame {

    private WebDriver driver;
    private String mayfairDiv = "//section[@class = 'page__content search__content']";
    private String moreXpath = "//div[@class = 'sc-ifAKCX ixOsdk' and contains(.,'More ')]";


    public VerifyFoundedGame(WebDriver driver) {
        this.driver = driver;
    }

    public void hoverOverElement(String searchString) {

        WebDriverWait webDriverWait = new WebDriverWait(driver, 5);

        webDriverWait.until(w -> w.findElement(By.xpath(mayfairDiv)));
        try{
            webDriverWait.until(w -> w.findElement(By.xpath("//img[@alt='" + searchString + "']")));
        } catch (WebDriverException e){
            throw new SkipException("game was not found on the website");
        }

        Actions moveOver = new Actions(driver);
        WebElement we = driver.findElement(By.xpath(mayfairDiv));
        moveOver.moveToElement(we).moveToElement(driver.findElement(By.xpath("//img[@alt='" + searchString + "']"))).perform();
        moveOver = new Actions(driver);

        try {

            moveOver.moveToElement(webDriverWait.until(w -> w.findElement(By.xpath(moreXpath)))).click().perform();

        } catch (WebDriverException e) {

            moveOver.moveToElement(we).moveToElement(driver.findElement(By.xpath("//img[@alt='" + searchString + "']"))).click().perform();
        }
    }

}
