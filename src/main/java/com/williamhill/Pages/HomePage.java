package com.williamhill.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    private WebDriver driver;
    private String magnifierXpath = "//button[@class = 'btn-search-magnifier']";
    private String magnifierXpathInput = "//input[@data-test = 'game-search-field']";

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickonMagniferButton() {

        WebDriverWait webDriverWait = new WebDriverWait(driver, 5);
        webDriverWait.until(w -> w.findElement(By.xpath(magnifierXpath)));
        driver.findElement(By.xpath(magnifierXpath)).click();
    }

    public void typeGameName(String searchString) {

        WebDriverWait webDriverWait = new WebDriverWait(driver, 5);
        webDriverWait.until(w -> w.findElement(By.xpath(magnifierXpathInput)));
        driver.findElement(By.xpath(magnifierXpathInput)).sendKeys(searchString);
    }
}
