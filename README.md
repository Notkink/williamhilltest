# WILLIAMHILL TEST

#Requirements:
1. You need to have at least java 1.8 installed.
2. For Chrome option Chrome need to be installed.
3. For Firefox option Firefox need to be installed.

#How to run
1. You need to download .zip file from the repository.
2. Extract .zip file to the chosen location.
3. Go to the ```target``` directory.
4. Open console within ```target``` directory.
5. run ```java -cp WilliamHillTest.jar org.testng.TestNG suites/testng.xml```
    
    *In addition you can provide parameters like: gameName or device
    
    *Example ```java -cp WilliamHillTest.jar -DgameName="Mayfair Roulette" -Ddevice="Chrome" org.testng.TestNG suites/testng.xml```
    
    *default gameName parameter is "Mayfair Roulette"
    
    *default device parameter is Chrome (you can only choose from : Chrome, iPhone, Android, Firefox
    
6. Output results are provided under ```target\test-output\WilliamHillWebPage``` directory.



